#!/usr/bin/python3

import dbus
bus = dbus.SessionBus()

display_config_well_known_name = "org.gnome.Mutter.DisplayConfig"
display_config_object_path = "/org/gnome/Mutter/DisplayConfig"

display_config_proxy = bus.get_object(display_config_well_known_name, display_config_object_path)
display_config_interface = dbus.Interface(display_config_proxy, dbus_interface=display_config_well_known_name)

serial, physical_monitors, logical_monitors, properties = display_config_interface.GetCurrentState()

mon = [0] * 2
freq = [0] * 2
count = 0
highestResolution = "0"

def get_resolution(input_string):
    at_index = input_string.find("@")
    if at_index == -1:
        raise ValueError("The input string does not contain an '@' sign.")
    return input_string[:at_index]


def getHighestCommonResolution(monitors):
    monitor_info1,modes1,props1 = monitors[0]
    monitor_info2,modes2,props2 = monitors[1]
    for monRes in modes1:
        for monRes2 in modes2:
            if (get_resolution(monRes[0]) == get_resolution(monRes2[0]) and ("i" not in monRes[0])):  #ignore interlaced mode 
                highestResolution=get_resolution(monRes[0])
                print("-----HIGHEST RESOLUTION-------")
                print(highestResolution)
                return highestResolution


def get_monitor( monitor,highestResolution):
    monitor_info, modes, props = monitor
    mon[count] = monitor_info[0]
    for mode in modes:
      #print(mode[0])
      if (get_resolution(mode[0]) == highestResolution):
        freq[count] = mode[0]
        break

if highestResolution == "0":
    highestResolution = getHighestCommonResolution(physical_monitors)

for monitor in physical_monitors:
  get_monitor(monitor,highestResolution)
  count = count + 1
print("-----RESULT---------")
print(mon[0])
print(freq[0])
print(mon[1])
print(freq[1])
print("--------------------")



mons = [
dbus.Struct(
    (
        dbus.Int32(0),
        dbus.Int32(0),
        dbus.Double(1.0),
        dbus.UInt32(0),
        dbus.Boolean(True),
        dbus.Array(
            [
                dbus.Struct(
                    (
                        dbus.String("eDP-1"),
                        dbus.String("CMN"),
                        dbus.String("0x14c9"),
                        dbus.String("0x00000000"),
                    ),
                    signature=None,
                ),
                dbus.Struct(
                    (
                        dbus.String("DP-2"),
                        dbus.String("HWP"),
                        dbus.String("HP ZR2440w"),
                        dbus.String("CN42150RG8"),
                    ),
                    signature=None,
                ),
            ],
            signature=dbus.Signature("(ssss)"),
        ),
        dbus.Dictionary({}, signature=dbus.Signature("sv")),
    ),
    signature=None,
)

]

updated_logical_monitors = [
        dbus.Struct(
            (
                dbus.Int32(0),
                dbus.Int32(0),
                dbus.Double(1.0),
                dbus.UInt32(0),
                dbus.Boolean(True),
                [
                    dbus.Struct(
                        #(dbus.String(mon[1]), dbus.String("1920x1080@48.002"), {}),
                        (dbus.String(mon[0]), dbus.String(freq[0]), {}),
                        signature=None,
                    ),
                     dbus.Struct(
                        #(dbus.String(mon[0]), dbus.String("1920x1080@50.000"), {}),
                        (dbus.String(mon[1]), dbus.String(freq[1]), {}),
                        signature=None,
                    )
                ],
            ),
            signature=None,
        ),

        
    ]



properties_to_apply = { "layout_mode": properties.get("layout-mode")}

method = 1 # 2 means show a prompt before applying settings; 1 means instantly apply settings without prompt

display_config_interface.ApplyMonitorsConfig(dbus.UInt32(serial), dbus.UInt32(method), updated_logical_monitors, properties_to_apply)